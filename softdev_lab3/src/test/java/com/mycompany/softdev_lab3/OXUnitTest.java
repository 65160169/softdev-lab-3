/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.softdev_lab3;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Asus
 */
public class OXUnitTest {
    
    public OXUnitTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testCheckwinRow1_O_true(){
        char[][] table = {{'O','O','O'},{'_','_','_'},{'_','_','_'}};
        char current = 'O';
        boolean result = Softdev_lab3.checkwin(table,current);
        assertEquals(true,result);
    }
    @Test
    public void testCheckwinRow2_O_true(){
        char[][] table = {{'_','_','_'},{'O','O','O'},{'_','_','_'}};
        char current = 'O';
        boolean result = Softdev_lab3.checkwin(table,current);
        assertEquals(true,result);
    }
    @Test
    public void testCheckwinRow3_O_true(){
        char[][] table = {{'_','_','_'},{'_','_','_'},{'O','O','O'}};
        char current = 'O';
        boolean result = Softdev_lab3.checkwin(table,current);
        assertEquals(true,result);
    }
    @Test
    public void testCheckwinRow1_X_true(){
        char[][] table = {{'X','X','X'},{'_','_','_'},{'_','_','_'}};
        char current = 'X';
        boolean result = Softdev_lab3.checkwin(table,current);
        assertEquals(true,result);
    }
    @Test
    public void testCheckwinRow2_X_true(){
        char[][] table = {{'_','_','_'},{'X','X','X'},{'_','_','_'}};
        char current = 'X';
        boolean result = Softdev_lab3.checkwin(table,current);
        assertEquals(true,result);
    }
    @Test
    public void testCheckwinRow3_X_true(){
        char[][] table = {{'_','_','_'},{'_','_','_'},{'X','X','X'}};
        char current = 'X';
        boolean result = Softdev_lab3.checkwin(table,current);
        assertEquals(true,result);
    }
    @Test
    public void testCheckwinCol1_O_true(){
        char[][] table = {{'O','_','_'},{'O','_','_'},{'O','_','_'}};
        char current = 'O';
        boolean result = Softdev_lab3.checkwin(table,current);
        assertEquals(true,result);
    }
    @Test
    public void testCheckwinCol2_O_true(){
        char[][] table = {{'_','O','_'},{'_','O','_'},{'_','O','_'}};
        char current = 'O';
        boolean result = Softdev_lab3.checkwin(table,current);
        assertEquals(true,result);
    }
    public void testCheckwinCol3_O_true(){
        char[][] table = {{'_','_','O'},{'_','_','O'},{'_','_','O'}};
        char current = 'O';
        boolean result = Softdev_lab3.checkwin(table,current);
        assertEquals(true,result);
    }
    @Test
    public void testCheckwinCol1_X_true(){
        char[][] table = {{'X','_','_'},{'X','_','_'},{'X','_','_'}};
        char current = 'X';
        boolean result = Softdev_lab3.checkwin(table,current);
        assertEquals(true,result);
    }
    @Test
    public void testCheckwinCol2_X_true(){
        char[][] table = {{'_','X','_'},{'_','X','_'},{'_','X','_'}};
        char current = 'X';
        boolean result = Softdev_lab3.checkwin(table,current);
        assertEquals(true,result);
    }
    public void testCheckwinCol3_X_true(){
        char[][] table = {{'_','_','X'},{'_','_','X'},{'_','_','X'}};
        char current = 'X';
        boolean result = Softdev_lab3.checkwin(table,current);
        assertEquals(true,result);
    }
    @Test
    public void testCheckwinbackslash_O_true(){
        char[][] table = {{'O','_','_'},{'_','O','_'},{'_','_','O'}};
        char current = 'O';
        boolean result = Softdev_lab3.checkwin(table,current);
        assertEquals(true,result);
    }
    @Test
    public void testCheckwinslash_O_true(){
        char[][] table = {{'_','_','O'},{'_','O','_'},{'O','_','_'}};
        char current = 'O';
        boolean result = Softdev_lab3.checkwin(table,current);
        assertEquals(true,result);
    }
    @Test
    public void testCheckwinbackslash_X_true(){
        char[][] table = {{'X','_','_'},{'_','X','_'},{'_','_','X'}};
        char current = 'X';
        boolean result = Softdev_lab3.checkwin(table,current);
        assertEquals(true,result);
    }
    @Test
    public void testCheckwinslash_X_true(){
        char[][] table = {{'_','_','X'},{'_','X','_'},{'X','_','_'}};
        char current = 'X';
        boolean result = Softdev_lab3.checkwin(table,current);
        assertEquals(true,result);
    }
    @Test
    public void testCheckdraw_false(){
        char[][] tb2 = Softdev_lab3.gettable();
        boolean result = Softdev_lab3.checkdraw(tb2);
        assertEquals(false,result);
    }
}
