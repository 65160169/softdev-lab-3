/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdev_lab3;

/**
 *
 * @author Asus
 */
class OX {

    static boolean checkwin(char[][] table, char current) {
        for(int row=0;row<3;row++){
            if(checkrow(table,current,row)){
                return true;
            }
        }
        for(int col=0;col<3;col++){
            if(checkcol(table,current,col)){
                return true;
            }
        }
        if(checkbackslash(table,current)){
            return true;
        }
        if(checkslash(table,current)){
            return true;
        }
        return false;
    }

    private static boolean checkrow(char[][] table, char current, int row) {
        return table[row][0] == current && table[row][1] == current && table[row][2] == current;
    }

    private static boolean checkcol(char[][] table, char current, int col) {
       return table[0][col] == current && table[1][col] == current && table[2][col] == current;
    }
    
    private static boolean checkbackslash(char[][] table, char current) {
       return table[0][0] == current && table[1][1] == current && table[2][2] == current;
    }
    
    private static boolean checkslash(char[][] table, char current) {
       return table[0][2] == current && table[1][1] == current && table[2][0] == current;
    }
    
    static boolean checkdraw(char[][] table) {
       return table[0][0] != '_' && table[0][1] != '_' && table[0][2] != '_' &&
              table[1][0] != '_' && table[1][1] != '_' && table[1][2] != '_' &&
              table[2][0] != '_' && table[2][1] != '_' && table[2][2] != '_';
    }
    
}
