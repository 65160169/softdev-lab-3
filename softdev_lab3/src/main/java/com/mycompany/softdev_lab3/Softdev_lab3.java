/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.softdev_lab3;
import java.util.Scanner;

/**
 *
 * @author Asus
 */
public class Softdev_lab3 {
    static boolean status = true;
    static char[][] table = {{'_','_','_'},{'_','_','_'},{'_','_','_'}};
    static char ox = 'X';
    static int count = 0;
    
    public static char[][] gettable(){
        return table;
    }
    
    public static void addox(char[][] table,char current){
        Scanner kb = new Scanner(System.in);
        System.out.print("Turn "+ox+" Please input row,col: ");
        int row = kb.nextInt();
        int col = kb.nextInt();
        if(table[row][col] == '_'){
            table[row][col] = current;
            count += 1;
        }else{
            addox(table,current);
        }
    }
    public static void switchox(char current){
        if(current=='O'){
            ox = 'X';
        }else if(current=='X'){
            ox = 'O';
        }
    }
    
    public static void showtable(char[][] table){
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                System.out.print(table[i][j]+" ");
            }
            System.out.println();
        }
    } 
    
    static boolean checkdraw1(char[][] table){
        if(checkdraw2(table)==true){
            return true;
        }
        return false;
    }
    
    static boolean checkdraw2(char[][] table) {
       return table[0][0] != '_' && table[0][1] != '_' && table[0][2] != '_' &&
              table[1][0] != '_' && table[1][1] != '_' && table[1][2] != '_' &&
              table[2][0] != '_' && table[2][1] != '_' && table[2][2] != '_';
    }
    
    static boolean checkwin(char[][] table, char current) {
        for(int row=0;row<3;row++){
            if(checkrow(table,current,row)){
                return true;
            }
        }
        for(int col=0;col<3;col++){
            if(checkcol(table,current,col)){
                return true;
            }
        }
        if(checkbackslash(table,current)){
            return true;
        }
        if(checkslash(table,current)){
            return true;
        }
        return false;
    }

    private static boolean checkrow(char[][] table, char current, int row) {
        return table[row][0] == current && table[row][1] == current && table[row][2] == current;
    }

    private static boolean checkcol(char[][] table, char current, int col) {
       return table[0][col] == current && table[1][col] == current && table[2][col] == current;
    }
    
    private static boolean checkbackslash(char[][] table, char current) {
       return table[0][0] == current && table[1][1] == current && table[2][2] == current;
    }
    
    private static boolean checkslash(char[][] table, char current) {
       return table[0][2] == current && table[1][1] == current && table[2][0] == current;
    }
    
    public static void reset(){
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                table[i][j] = '_';
            }
        }
        ox = 'X';count = 0;
    } 
    
    public static boolean checkcontinue(){
        Scanner kb = new Scanner(System.in);
        System.out.print("Continue?(y/n): ");
        String yn = kb.next();
        if(yn.equals("y")){
            reset();
            System.out.println("New Game started");
            return status = true;
        }
        else if(yn.equals("n")){
            return status = false;
        }
        return false;
    }
    
    public static void main(String[] args) {
        System.out.println("Welcome To My OX Games new updates!!");
        while(status){
            showtable(table);
            addox(table,ox);
            if(checkwin(table,ox)){
                showtable(table);
                System.out.println("The Winner is "+ox);
                checkcontinue();
            }else if(checkdraw1(table)){
                showtable(table);
                System.out.println("<<<Draw>>>");
                checkcontinue();
            }
            switchox(ox);
        }
    }
}
